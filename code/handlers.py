from telegram.update import Update
from telegram.ext.callbackcontext import CallbackContext
import config
import ast
from response_strings import *

db=config.db

def post(update: Update, founded:dict, showtags:bool=False):
    for d in founded:
        if 'text' in d["data"]:
            update.message.reply_text(d["data"]['text'])
        else:
            caption=d["data"]['caption']
            if 'photo_id' in d["data"]:
                update.message.reply_photo(photo=d["data"]['photo_id'],
                                           caption=caption)
            else:
                update.message.reply_document(document=d["data"]['file_id'],
                                           caption=caption)
        if showtags:
            update.message.reply_text( str(d['tags'])+"\n"+str(d['time']) )

        

def start(update: Update, context: CallbackContext):
	update.message.reply_text(text_start)

def help(update: Update, context: CallbackContext):
    if not context.args:
        update.message.reply_text(text_help)
        return

    if context.args[0]=='пошук':
        update.message.reply_text(text_help_search)
    elif context.args[0]=='додавання':
            update.message.reply_text(text_help_add)

def search(update: Update, context: CallbackContext):
    try:
       inputTags=ast.literal_eval('{'+' '.join(context.args)+'}')
    except (ValueError,SyntaxError) as e:
        update.message.reply_text(str(e))
        return

    stuffToSearch={}
    knownTags=db.getKnownTags(update.message.chat_id)
    showtags=False
    for k in inputTags.keys():
        if k in ("time","_time"):
            stuffToSearch['time']=inputTags[k]
            continue
        if k in ("_showtags","showtags"):
            showtags=inputTags[k]
            continue
        if k.startswith('_'):
            stuffToSearch['data.'+k.lstrip('_')]=inputTags[k]
            continue
        if k not in knownTags:
            update.message.reply_text(f"Пошук з неіснуючим ключем {k}")
        stuffToSearch["tags."+k]=inputTags[k]

    #print(stuffToSearch)
    return (stuffToSearch,showtags)

def searchNpost(update: Update, context: CallbackContext):
    stuffToSearch,showtags=search(update, context)
    print(stuffToSearch)
    founded=list(db.find(update.message.chat_id, stuffToSearch))
    update.message.reply_text(f"Знайдено {len(founded)}")
    post(update, founded,showtags)


def delete(update: Update, context: CallbackContext):
    stuffToDelete,_=search(update, context)
    amount=db.delete(update.message.chat_id, stuffToDelete)
    update.message.reply_text(f"Видалено {amount} елементів")

def general(update: Update, context: CallbackContext):
    update.message.reply_text(update.message.text or update.message.caption 
                              or "No caption")
    if update.message.media_group_id:
        update.message.reply_text("группування поки не підтримується\n"
                                + "робота буде лише з 1им фото/файлом")
    
def tagsReply(update: Update, context: CallbackContext):
    infoMessage=update.message.reply_to_message
    data={}
    if infoMessage.text:
        data['text']=infoMessage.text
    else:
        data['type']=type(infoMessage.effective_attachment).__name__
        data['caption']=infoMessage.caption
        if infoMessage.photo: # photo as list of different sizes
            data['photo_id']=infoMessage.photo[3].file_id
        else:
            data['file_id']=infoMessage.effective_attachment.file_id

    try:
        tags=ast.literal_eval('{'+update.message.text+'}')
    except (ValueError,SyntaxError) as e:
        update.message.reply_text(str(e))
        return

    if len(tags)==0:
        return
    update.message.reply_text("Буде збережено з тегами:"+ str(tags))
    db.insert(infoMessage.chat_id, data, tags)
