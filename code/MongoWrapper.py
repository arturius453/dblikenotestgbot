from pymongo import MongoClient
from datetime import datetime

class MongoWrapper:
    def __init__(self, connStr):
        self.client = MongoClient(connStr)
        self.db = self.client["notes"] 

    def insert(self,id, data:dict,tags:dict):
        collection=self.db[str(id)]
        collection.insert_one({'data':data,"tags":tags,"time":datetime.now()})
        #collection.create_index({"$**": "text"})

    def getKnownTags(self,id):
        collection=self.db[str(id)]
        tags=[]
        for dictionary in collection.distinct('tags'):
            tags.extend(dictionary.keys())
        return tags

    def find(self,id, stuffToSearch:dict):
       collection=self.db[str(id)]
       return collection.find(stuffToSearch)

    def delete(self,id, stuffToDelete:dict):
       collection=self.db[str(id)]
       return collection.delete_many(stuffToDelete).deleted_count
