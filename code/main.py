import config
from telegram.ext.commandhandler import CommandHandler
from telegram.ext.messagehandler import MessageHandler
from telegram.ext.filters import Filters
from handlers import *

updater=config.updater

updater.dispatcher.add_handler(CommandHandler('start', start))
updater.dispatcher.add_handler(CommandHandler('help', help))
updater.dispatcher.add_handler(CommandHandler('search', searchNpost))
updater.dispatcher.add_handler(CommandHandler('delete', delete))
updater.dispatcher.add_handler(MessageHandler(Filters.reply, tagsReply))
updater.dispatcher.add_handler(MessageHandler(Filters.all, general))
updater.start_polling()
updater.idle()

