from MongoWrapper import MongoWrapper
from telegram.ext.updater import Updater

file=open("config")
TOKEN=file.readline().removeprefix("TOKEN=").rstrip()
mongodbConStr=file.readline().removeprefix("MONGODB_CONSTR=").rstrip()
file.close()

db=MongoWrapper(mongodbConStr)

updater = Updater(TOKEN,
				use_context=True)
